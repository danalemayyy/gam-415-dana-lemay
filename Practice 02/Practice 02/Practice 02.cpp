#include <iostream>
#include <map>
#include <vector>
#include <functional>
using namespace std;

//returns value
int FindIf(vector<int> v, function<bool(int)> f)
{
	for (int value: v)
	{
		if (f(value))
			return value;
	}
	throw std::invalid_argument("none evaluates to true");
}

//returns index
int IndexOf(vector<int> v, function<bool(int)> f)
{
	int i = 0;
	for (int value : v)
	{
		if (f(value))
			return i;
		i++;
	}
	throw std::invalid_argument("none evaluates to true");
}

vector<int> Filter(vector<int> v, function<bool(int, vector<int>)> f)
{
	vector<int> temp;
	for (int value : v)
	{
		if (f(value, temp))
			temp.push_back(value);
	}
	return temp;
}

vector<int> Map(vector<int> v, function<int(int)> f)
{
	vector<int> temp;
	for (int value : v)
	{
		temp.push_back(f(value));
	}
	return temp;
}

int ReduceLeft(vector<int> v, int initial, function<int(int, int)> f) 
{
	for(int index = 1; index < v.size(); index++)
	{
		initial = f(initial, v[index]);
	}
	return initial;
}

function<bool(int)> Negate(function<bool(int)> f)
{
	return [f](int x)->bool {return !f(x); };
}

int main()
{
	std::vector<int> v{ 9,5,5,3,2,1,4,0 };

	cout << "FindIf (3)" << endl;
	cout << FindIf(v, [](int i) {if (i == 3) return true; else return false; }) << endl;
	cout << endl;

	cout << "IndexOf (3)" << endl;
	cout << FindIf(v, [](int i) {if (i == 3) return true; else return false; }) << endl;
	cout << endl;

	cout << "Filter" << endl;

	v = Filter(v, [](int i, vector<int> v) {for (int value : v) {
		if (value == i) return false;
	} return true;
	});

	for (int value : v)
	{
		cout << value << " ";
	}
	cout << endl;
	cout << endl;

	cout << "Map (*2)" << endl;
	v = Map(v, [](int i) {return i * 2; });

	for (int value : v)
	{
		cout << value << " ";
	}
	cout << endl;
	cout << endl;

	cout << "ReduceLeft" << endl;
	cout << ReduceLeft(v, v[0], [](int initial, int next) {return initial + next; });
	cout << endl;

	cout << "Negate" << endl;
	function<bool(int)> lambda = [](int i) {if (i <= 0) return true; return false; };
	cout << "before: " << lambda(3) << endl;
	lambda = Negate(lambda);
	cout << "after: " << lambda(3) << endl;
	cout << endl;

	cin.get();
	return 0;
}