#pragma once

#include <string>
#include <functional>



#ifndef DLL_EXPORT

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#endif



DLL_EXPORT bool SG_Initialize(const std::string title, int width, int height);
DLL_EXPORT void SG_CleanUp();
DLL_EXPORT void SG_SetPixelDelay(uint32_t milliseconds);

DLL_EXPORT void SG_Run(std::function<void(float)> renderCallback);
DLL_EXPORT void SG_SetPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue);

class DLL_EXPORT Image
{
public:
	Image(int newWidth, int newHeight);

	Image(const std::string& filename);

	Image(const Image& other) = delete;

	~Image();

	Image& operator=(const Image& other) = delete;


	void Free();

	int GetWidth() const;

	int GetHeight() const;

	void SetAsTarget();

	void UnsetAsTarget();

	// Warning: Very unoptimized/slow to render individual pixels
	void SetPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue);

	void DrawToScreen(float x, float y);

	void DrawToScreen(float x, float y, float degrees);

private:
	int width, height;

	struct pimpl;
	pimpl* impl;
};