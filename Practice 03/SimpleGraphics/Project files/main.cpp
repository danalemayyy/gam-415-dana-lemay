#include <iostream>
#include <string>
#include "SimpleGraphics.h"
#include "windows.h"
#define PI 3.14159265
using namespace std;



class Sprite
{
public:
	float x, y;
	float degrees;
	Image image;

	Sprite(const std::string& filename)
		: image(filename)
	{
		x = 0.0f;
		y = 0.0f;
		degrees = 0.0f;
	}

	void Draw()
	{
		image.DrawToScreen(x, y, degrees);
	}
};



void DrawGradient()
{
	int numRows = 100;
	int numColumns = 100;
	for (int row = 0; row < numRows; ++row)
	{
		for (int column = 0; column < numColumns; ++column)
		{
			uint8_t red = (float(row) / numRows) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			SG_SetPixel(column, row, red, green, blue);
		}
	}
}

void FillImageWithGradient(Image& image)
{
	image.SetAsTarget();

	for (int row = 0; row < image.GetHeight(); ++row)
	{
		for (int column = 0; column < image.GetWidth(); ++column)
		{
			uint8_t red = (float(row) / image.GetHeight()) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			image.SetPixel(column, row, red, green, blue);
		}
	}

	image.UnsetAsTarget();
}





int main(int argc, char* argv[])
{
	int maxX = 800;
	int maxY = 600;

	SG_Initialize("Image test", 800, 600);

	SG_SetPixelDelay(1);


	Sprite smile("smile.bmp");

	SG_Run([&smile, maxX, maxY](float dt)
		{
			//FillImageWithGradient(smile.image);
			//smile.Draw();
			//DrawGradient();

			for (int x = -400; x < 400; x++)
			{
				//SG_SetPixel(100, 50, 255, 0, 0);
				SG_SetPixel(x + 400, 300, 255, 255, 0);
			}

			for (int y = -300; y < 300; y++)
			{
				SG_SetPixel(400, y + 300, 255, 255, 0);
			}

			for (int x = -400; x < 400; x++)
			{
				SG_SetPixel(x + 400, (maxY - (2 * (x)+3)) - 300, 255, 0, 0);
			}

			for (int x = -400; x < 400; x++)
			{
				SG_SetPixel(x + 400, (maxY - (3 * x * x + 4 * x + 5)) - 300, 0, 255, 0);
			}

			for (int x = -400; x < 400; x++)
			{
				float temp = ((3 * (x + 4)) + 5) * PI / 180;
				SG_SetPixel(x + 400, maxY - ((2 * sin(temp)) + 6)-300, 0, 255, 255);
		}

		for (int x = -400; x < 400; x++)
		{
			SG_SetPixel(x + 400, maxY - 5-300, 255, 255, 255);
		}
	});

	smile.image.Free();

	SG_CleanUp();

	return 0;
}