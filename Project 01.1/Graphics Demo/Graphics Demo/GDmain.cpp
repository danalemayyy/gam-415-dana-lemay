#include <iostream>
#include <string>
#include "GraphicsLibrary.h"
using namespace std;


void Draw(float pixalDelay)
{
	//BresenhamLine(100, 150, 200, 250, 250, 0, 0);
	//Triangle(10, 20, 100, 120, 200, 275, 0, 250, 0);
	Circle(200, 200, 100, 250, 0, 250);
	Rectangle(200, 50, 200,250, 250, 0, 250);
	Circle(200, 400, 100, 100, 50, 0);
	Circle(150, 375, 20, 250, 250, 250);
	Circle(250, 375, 20, 250, 250, 250);
	BresenhamLine(200, 375, 250, 400, 100, 50, 0);
	BresenhamLine(200, 425, 250, 400, 100, 50, 0);
	Rectangle(125, 50, 150, 425, 100, 50, 0);
	Rectangle(25, 50, 150, 425, 250, 250, 250);
	Rectangle(25, 50, 175, 425, 250, 250, 250);
	Rectangle(25, 50, 200, 425, 250, 250, 250);
	Rectangle(25, 50, 225, 425, 250, 250, 250);
	Rectangle(25, 50, 250, 425, 250, 250, 250);
	HorizontalLine(125, 150, 450, 250, 250, 250);
	Circle(200, 700, 200, 250, 0, 250);
	HorizontalLine(50, 175, 175, 200, 0, 250);
	VerticalLine(100, 175, 175, 200, 0, 250);
	Rectangle(25, 50, 250, 500, 250, 0, 250);
	Rectangle(25, 50, 100, 500, 250, 0, 250);
	Rectangle(300, 400, 50, 550, 250, 0, 250);

}

int main()
{
	StartRendering(800, 800, 0, Draw);
	return 0;
}