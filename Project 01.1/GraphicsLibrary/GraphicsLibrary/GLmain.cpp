#include <string>
#include <functional>
#include "GraphicsLibrary.h"
#include "SimpleGraphics.h"

void StartRendering(int width, int height, int pixelDelay, const std::function<void(float)> renderCallback)
{
	SG_Initialize("Wahluigi (fear him)", width, height);
	SG_SetPixelDelay(pixelDelay);
	SG_Run(renderCallback);
	SG_CleanUp();
}
//SG_SetPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue);

//x left right; y down pos
void HorizontalLine(int length, int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
	for (int i = 0; i < length; i++)
	{
		SG_SetPixel(x+i, y, red, green, blue);	
	}
}

void VerticalLine(int length, int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
	for (int i = 0; i < length; i++)
	{
		SG_SetPixel(x, y+i, red, green, blue);
	}
}

//Rectangle(50, 200, 100, 100, 0, 250, 0);
void Rectangle(int xlength, int ylength, int xpos, int ypos, uint8_t red, uint8_t green, uint8_t blue)
{
	HorizontalLine(xlength, xpos, ypos, red, green, blue);
	HorizontalLine(xlength, xpos, ypos+ ylength, red, green, blue);
	VerticalLine(ylength, xpos, ypos, red, green, blue);
	VerticalLine(ylength, xpos + xlength, ypos, red, green, blue);
}


void FilledRectangle(int xlength, int ylength, int xpos, int ypos, uint8_t red, uint8_t green, uint8_t blue)
{
	Rectangle(xlength, ylength, xpos, ypos, red, green, blue);
	for (int i = 0; i < ylength; i++)
	{
		HorizontalLine(xlength, xpos, ypos+i, red, green, blue);
	}
}

void BresenhamLine(int x0, int y0, int x1, int y1, uint8_t red, uint8_t green, uint8_t blue)
{
	float dx = abs(x1 - x0);
	float sx = x0 < x1 ? 1 : -1;
	float dy = -abs(y1 - y0);
	float sy = y0 < y1 ? 1 : -1;

	float error = dx + dy;
	while(true)
	{
		SG_SetPixel(x0, y0, red, green, blue);

		if (x0 == x1 && y0 == y1) 
			break;

		float e2 = 2 * error;

		if (e2 >= dy)
		{
			error += dy;
			x0 += sx;
		}
		if (e2 <= dx)
		{
			error += dx;
			y0 += sy;
		}
	}
}

//Triangle(300, 200, 350, 10, 120, 275, 0, 250, 0);
void Triangle(int x1, int y1, int x2, int y2, int x3, int y3, uint8_t red, uint8_t green, uint8_t blue)
{
	BresenhamLine(x1, y1, x2, y2, red, green, blue);
	BresenhamLine(x1, y1, x3, y3, red, green, blue);
	BresenhamLine(x2, y2, x3, y3, red, green, blue);
}

void Circle(int xc, int yc, int radius, uint8_t red, uint8_t green, uint8_t blue)
{
	int x = radius;
	int y = 0;

    if (radius <= 0)
    {
		SG_SetPixel(x + xc, -y + yc, red, green, blue);
		SG_SetPixel(y + xc, x + yc, red, green, blue);
		SG_SetPixel(-y + xc, x + yc, red, green, blue);
		return;
    }

    int midPoint = 1 - radius;
    while (x > y)
    {
        y++;

        if (midPoint <= 0)
            midPoint = midPoint + 2 * y + 1;

        else
        {
            x--;
            midPoint = midPoint + 2 * y - 2 * x + 1;
        }

        if (x < y)
            break;

		SG_SetPixel(x + xc, y + yc, red, green, blue);
		SG_SetPixel(-x + xc, y + yc, red, green, blue);
		SG_SetPixel(x + xc, -y + yc, red, green, blue);
		SG_SetPixel(-x + xc, -y + yc, red, green, blue);

        if (x != y)
        {
			SG_SetPixel(y + xc, x + yc, red, green, blue);
			SG_SetPixel(-y + xc, x + yc, red, green, blue);
			SG_SetPixel(y + xc, -x + yc, red, green, blue);
			SG_SetPixel(-y + xc, -x + yc, red, green, blue);
        }
    }
}