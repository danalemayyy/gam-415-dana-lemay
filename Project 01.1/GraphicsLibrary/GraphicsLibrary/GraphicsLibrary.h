#pragma once
#include <string>
#include <functional>

#ifndef DLL_EXPORT
#ifdef BUILD_GRAPHICS_LIBRARY
#define DLL_EXPORT _declspec(dllexport)
#else
#define DLL_EXPORT _declspec(dllimport)
#endif
#endif

DLL_EXPORT void StartRendering(int width, int height, int pixelDelay, const std::function<void(float)> renderCallback);
DLL_EXPORT void HorizontalLine(int length, int x, int y, uint8_t red, uint8_t green, uint8_t blue);
DLL_EXPORT void VerticalLine(int length, int x, int y, uint8_t red, uint8_t green, uint8_t blue);
DLL_EXPORT void Rectangle(int xlength, int ylength, int xpos, int ypos, uint8_t red, uint8_t green, uint8_t blue);
DLL_EXPORT void FilledRectangle(int xlength, int ylength, int xpos, int ypos, uint8_t red, uint8_t green, uint8_t blue);
DLL_EXPORT void BresenhamLine(int x1, int y1, int x2, int y2, uint8_t red, uint8_t green, uint8_t blue);
DLL_EXPORT void Triangle(int x1, int y1, int x2, int y2, int x3, int y3, uint8_t red, uint8_t green, uint8_t blue);

DLL_EXPORT void Circle(int x, int y, int radius, uint8_t red, uint8_t green, uint8_t blue);
//DLL_EXPORT void FilledCircle(int x, int y, int radius, uint8_t red, uint8_t green, uint8_t blue);